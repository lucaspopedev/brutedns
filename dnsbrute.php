<?php

error_reporting(0);

$wordlist = $argv[2];
$domain = $argv[1];

if (empty($wordlist) || empty($domain)) {
    echo "Usage: php dnsbrute.php <domain.com> <wordlist.txt>" . PHP_EOL;
    exit(1);
}

/**
 * Utiliza a wordlist para tentar resolver os possíveis subdomínios afim de descobrir se são válidos.
 *
 * @param string $wordlist
 * @param string $domain
 * @return array
 */
function getDnsLinks(string $wordlist, string $domain)
{    
    if (file_exists($wordlist) && is_file($wordlist)) {

        $links = [];

        $openWordList = fopen($wordlist, "r");
    
        while (!feof($openWordList)) {
    
            $subdomain = trim(fgets($openWordList));
            
            if (!empty($subdomain)) {
                
                $dnsResolved  = dns_get_record("{$subdomain}.{$domain}", DNS_A);
                
                if (!empty($dnsResolved)) {
                    $links = $links + [$dnsResolved[0]['host'] => $dnsResolved[0]['ip']];
                }
            }
    
        }
    
        return array_unique($links);
    
    } else {
        die("Wordlist não encontrada!");
    }
}

/**
 * Exibe os domínios encontrados.
 *
 * @param array $links
 * @param string $domain
 * @return void
 */
function showFoundLinks(array $links, string $domain)
{
    echo PHP_EOL . "Domínios encontrados:" . PHP_EOL;
    echo "---------------------" . PHP_EOL;
    foreach ($links as $key => $value) {
        echo "[+] {$key} => {$value}" . PHP_EOL;
    }
    echo "---------------------" . PHP_EOL;
    echo "Total de domínios encontrados: " . count($links) . PHP_EOL;
    echo "Domínio principal: {$domain}" . PHP_EOL;
}

showFoundLinks(getDnsLinks($wordlist, $domain), $domain);